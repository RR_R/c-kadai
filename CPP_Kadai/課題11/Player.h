#pragma once
class Player
{
	int hp,atk,damage,def,dead;

public:
		Player();
		void DispHp();
		int Attack(int i);
		void Damage(int i);
		int GetDef();
		bool IsDead();
};