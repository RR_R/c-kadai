class Enemy
{
	int hp, atk, damage, def, dead;

public:
		Enemy();
		void DispHp();
		int Attack(int i);
		void Damage(int i);
		int GetDef();
		bool IsDead();
};	
