/*
Sample.cpp
SampleClass クラスの各メンバ関数を定義
*/
//ヘッダをインクルード
#include <iostream>
#include "Figure.h" //クラスを宣言しているヘッダ
//メンバ変数に値を代入するメンバ関数
void Figure::SetTeihen(float f) {
	teihen = 10.0f;
}
//メンバ変数同士の計算を行うメンバ関数
void Figure::SetTakasa(float f) {
	takasa = 5.5f;
}
//メンバ変数の内容を出力するメンバ関数
void Figure::Disp() {
	std::cout << "面積は" << menseki << "\n";
}