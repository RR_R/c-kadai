#include "Status.h"

void Status::SetLv(int i)
{
	lv = i; //7
}

void Status::Calc()
{
	hp = lv * lv + 50; //99
	atk = lv * 10; //70
	def = lv * 9; //63
}

int Status::GetHp()
{
	return hp;  //99
}
int Status::GetAtk()
{
	return atk;
}
int Status::GetDef()
{
	return def;
}